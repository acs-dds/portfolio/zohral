<?php

require_once __DIR__.'/mapper.php';

class LaipisseumController {
	private $mapper;

	public function __construct() {
		$this->mapper = new LaipisseumMapper();
	}

	public function genererParagrapheAction($texte) {
		return $this->mapper->genererParagraphe($texte);
	}
}