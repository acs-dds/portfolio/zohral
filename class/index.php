<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<h1>Générateur de Lauremme Aipisseum (Laipisseum pour les intimes)</h1>

<form>
	<label for="nb">Nombre de paragraphes :</label>
	<input type="number" name="nb" id="nb" value="1"/><br/>
	Parfums additionnels :
	<input type="checkbox" name="themes[]" value="zombies"/> Zombies
	<input type="checkbox" name="themes[]" value="glace"/> Glaces Häägen Dazs
	<input type="checkbox" name="themes[]" value="metal"/> Groupes de metal
	<input type="submit" value="Générer !"/>
</form>
<?php
	require_once __DIR__.'/classes/laipisseumcontroller.php';

	$c = new LaipisseumController();

	if (isset($_GET['nb'])): ?>
<h3>Et voilà</h3>
<?php
		if (isset($_GET['themes'])):
			echo $c->genererParagraphesAction($_GET['nb'], $_GET['themes']);
		else:
			echo $c->genererParagraphesAction($_GET['nb']);
		endif;
	else: ?>
<h3>En voici déjà un</h3>
<?php
	echo $c->genererParagrapheAction();
	endif;
?>
</body>
</html>
