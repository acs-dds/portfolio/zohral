<?php

header("Content-type: text/plain");

function frenchify($phrase) {
	$phrase = trim($phrase); // on commence par s'assurer qu'il n'y a pas d'espace en début ou fin de phrase
	$placeDuChameau = strrpos($phrase, ' chameau'); // on cherche le combo espace-chameau, pour être sûr qu'il y a bien chameau en bout de phrase et pas un autre mot qui terminerait par chameau
	$longueurDuChameau = strlen(' chameau'); // on calcule la longueur de ce combo espace-chameau
	$longueurDeLaPhrase = strlen($phrase); // on calcule la longueur de la phrase
	$phrase = ucfirst($phrase); // dans tous les cas, on met une majuscule au premier caractère
	if ($placeDuChameau == $longueurDeLaPhrase - $longueurDuChameau) { // si l'indice contenu dans $placeDuChameau est égal à la longueur de la phrase moins la longueur du combo, alors c'est que ce combo est situé tout à la fin de la phrase
		return $phrase." !"; // on ajoute donc un point d'exclamation
	} else {
		return $phrase."."; // sinon un point
	}
}

echo frenchify("bonjour, ma belle chamelle").PHP_EOL;
echo frenchify("bonjour, mon beau chameau");