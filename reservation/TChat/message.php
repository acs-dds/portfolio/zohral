<?php

class Message {
	private $author;
	private $date;
	private $content;

	public function __construct($a, $d, $m) {
		$this->author = $a;
		$this->date = $d;
		$this->content = $m;
	}

	public function toArray() {
		return [$this->author, $this->date, $this->content];
	}

	public function renderHtml() {
		return "<h2>{$this->author} a dit</h2> <p>{$this->content}</p> le <time>".strftime("%A %e %B %H:%M:%S", $this->date)."</time>";
	}
}