<!DOCTYPE html>
<html>
<head>
	<title>Achievo</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/font-awesome.min.css'); ?>">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,900|Pacifico" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/style.css'); ?>">
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container-fluid">
	    <div class="navbar-header">
	    	<h1 class="navbar-brand"><i class="fa fa-check-square-o" aria-hidden="true"></i> Achievo</h1>
	    </div>
	</div>
</nav>
<?php if (isset($utilisateur)): ?>
<section class="user">Hello, <?php echo $utilisateur; ?></section>
<?php endif; ?>
<div class="container">
	<div class="row">
