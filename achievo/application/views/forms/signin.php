<form action="<?php echo site_url('signin'); ?>" method="post">
  <div class="form-group">
    <label for="signin-login">Nom d'utilisateur</label>
    <input type="text" name="login" class="form-control" id="signin-login" placeholder="Pikachudu93">
  </div>
  <div class="form-group">
    <label for="signin-mdp">Mot de passe</label>
    <input type="password" name="mdp" class="form-control" id="signin-mdp" placeholder="représente...">
  </div>
  <button type="submit" class="btn btn-default">GO !</button>
  <button type="button" class="btn btn-link signup-switch">Pas encore de compte ?</button>
</form>
<?php if (isset($message)): ?><p class="error"><?php echo $message; ?></p><?php endif; ?>