<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function signin() {
		if ($this->session->has_userdata('utilisateur')) redirect('users/signout');
		if ($this->input->post('login')) {// si envoi de données
			$this->load->model('utilisateur_model');
			if (!is_null($u = $this->utilisateur_model->fetchUtilisateur(
				$this->input->post('login'),
				$this->input->post('mdp')
			))) {
				$this->session->utilisateur = $u;
				redirect('tchat');
			} else {
				$this->load->view('common/header');
				$this->load->view('forms/signin', ['message' => 'mauvais nom d\'utilisateur/mot de passe']);
				$this->load->view('common/footer');
			}
		} else {
			$this->load->view('common/header');
			$this->load->view('forms/signin');
			$this->load->view('common/footer');
		}
	}

	public function signout() {
		$this->session->unset_userdata('utilisateur');
		redirect('users/signin');
	}

	public function signup() {
		if ($this->session->has_userdata('utilisateur')) redirect('users/signout');
		if ($this->input->post('login')) {
			$this->load->model('utilisateur_model');
			$this->session->utilisateur = $this->utilisateur_model->saveUtilisateur(
				$this->input->post('login'),
				$this->input->post('mdp'),
				$this->input->post('nom'),
				$this->input->post('prenom')
			);
		}
	}
}
