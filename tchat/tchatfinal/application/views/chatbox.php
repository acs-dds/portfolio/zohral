<main id="tchatbox">
	<ul id="users-list">
	</ul>
	<dl id="message-list">
		<?php foreach($messages as $m): ?>
		<dt><?php echo $m['auteur']; ?></dt>
		<dd><?php echo $m['contenu']; ?></dd>
		<?php endforeach; ?>
	</dl>
	<textarea id="message-new"></textarea>
	<button id="message-post" type="button">Envoyer</button>
</main>