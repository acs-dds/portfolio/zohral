<?php

class Message_model extends CI_Model {

        public function fetchMessages($iddiscussion) {
                return $this->db->query("SELECT id, auteur, contenu, \"date\", idauteur FROM vuemessage WHERE iddiscussion = ? ORDER BY \"date\" ASC", [$iddiscussion])->result_array();
        }

        public function fetchMessagesSince($iddiscussion, $stamp) {
                return $this->db->query("SELECT id, auteur, contenu, \"date\", idauteur FROM vuemessage WHERE iddiscussion = ? AND \"date\" > to_timestamp(?) ORDER BY \"date\" ASC", [$iddiscussion, $stamp])->result_array();
        }

        public function saveMessage($contenu, $idauteur, $iddiscussion) {
                $this->db->query("INSERT INTO message (idutilisateur, iddiscussion, contenu) VALUES (?, ?, ?);", [$idauteur, $iddiscussion, $contenu]);
        }
}